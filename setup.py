#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('pge_welcome_plugin/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')


setup(
    name='pge-welcome-plugin',
    version=version,
    description="""Pyforms Generic Editor Welcome plugin""",
    author='Ricardo Jorge Vieira Ribeiro',
    author_email='ricardojvr@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/fchampalimaud/pycontrol-gui-plugin-welcome',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

    package_data={'pge_welcome_plugin': [
            'resources/*.*',
        ]
    },
)
