# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pysettings import conf

from pyforms import BaseWidget
from pyforms.Controls import ControlWeb

logger = logging.getLogger(__name__)


class WelcomeWindow(BaseWidget):
	"""
	See:
	- pyforms_generic_editor.models.projects.__init__.py
	- pyforms_generic_editor.models.projects.projects_window.py
	"""

	def __init__(self, projects):
		BaseWidget.__init__(self, 'Welcome')

		self.projects = projects

		self._web = ControlWeb()

		self._formset = [
			'_web',
		]

		self._web.value = conf.WELCOME_PLUGIN_URL

	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called

		self._locals = locals()
		self._globals = globals()

	def beforeClose(self):
		return False

	@property
	def mainwindow(self):
		return self.projects.mainwindow
