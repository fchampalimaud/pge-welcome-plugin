#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os


WELCOME_PLUGIN_ICON = os.path.join( os.path.dirname(__file__), 'resources', 'welcome.png' )

WELCOME_PLUGIN_WINDOW_SIZE = 800, 600

WELCOME_PLUGIN_URL = 'https://bitbucket.org/fchampalimaud/pyforms-generic-editor'