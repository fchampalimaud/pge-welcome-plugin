# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pysettings import conf

if conf.PYFORMS_USE_QT5:
	from PyQt5.QtGui import QIcon
else:
	from PyQt4.QtGui import QIcon

from pge_welcome_plugin.welcome_window import WelcomeWindow


class ProjectsWelcome(object):
	def __init__(self, mainwindow=None):
		super(ProjectsWelcome, self).__init__(mainwindow)
		self.open_welcome_plugin()

	def register_on_main_menu(self, mainmenu):
		super(ProjectsWelcome, self).register_on_main_menu(mainmenu)

		menu_index = 0
		for i, m in enumerate(mainmenu):
			if 'Window' in m.keys():
				menu_index = i
				break

		mainmenu[menu_index]['Window'].append('-')
		mainmenu[menu_index]['Window'].append(
			{'Welcome': self.open_welcome_plugin, 'icon': QIcon(conf.WELCOME_PLUGIN_ICON)})

	def open_welcome_plugin(self):
		if not hasattr(self, 'welcome_plugin'):
			self.welcome_plugin = WelcomeWindow(self)
			self.welcome_plugin.show()
			self.welcome_plugin.subwindow.resize(*conf.WELCOME_PLUGIN_WINDOW_SIZE)
		else:
			self.welcome_plugin.show()

		return self.welcome_plugin
