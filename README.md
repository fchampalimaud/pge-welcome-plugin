# Pyforms Generic Editor Welcome plugin

Show a welcome page on start up.

## How to install:

1. On your Desktop, create a new folder on your favorite location (e.g. "plugins")
2. Move this plugin folder to that location
3. Open GUI
4. Select Options > Edit user settings
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'pge_welcome_plugin',
    ]
    WELCOME_PLUGIN_URL = 'WEB URL TO DOCUMENTATION'
```
6. Save
7. Restart GUI